package lambda;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Processing {
	
	private static List<MonitoredData> l=new ArrayList<MonitoredData>();
	
	public Processing(){
		
	}
	
	public static List<MonitoredData> readFile() throws FileNotFoundException{
		Scanner s=new Scanner(new File("Activities.txt"));
		while(s.hasNext()){
			l.add(new MonitoredData(s.next(),s.next(),s.next(),s.next(),s.next()));
		}
		s.close();
		return l;
	}
	
	public static long task1(){
		return l.stream().map(a->a.getStartDate()).distinct().count();
		
	}
	
	public static Map<String,Long> task2(){
		return l.stream().collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting()));
	}
	
	public static Map<String, Map<String, Long>> task3(){
		return l.stream().collect(Collectors.groupingBy(MonitoredData::getStartDate, Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting())));
		
	}

	
	public static void task4(){
		Map<String,Integer>m=l.stream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingInt(MonitoredData::getDur)));
		try{
			Files.write(Paths.get("task4.txt"),()->m.entrySet()
					.stream()
					.filter(a -> a.getValue() > 600)
					.<CharSequence>map(b->b.getValue() + " "  +  "\n\n\n\n\n" + b.getKey()).iterator());
		}catch(IOException e){
			e.printStackTrace();
		}
		System.out.println(m);
	}
	
	public static void task5() throws IOException{
		List<String> t5=l.stream().map(a->a.getActivity()).distinct()
				.collect(Collectors.toCollection(ArrayList::new));
		
		Map<String,Long> subFive=new TreeMap<String,Long>();
		
		Map<String,Long> dur=new TreeMap<String,Long>();
		
		List<String> afis;
		
		t5.stream().forEach(a-> {
			Long num=l.stream().filter(b->b.getActivity().equals(a)).filter(b->b.getDur()<300).count();
			
			subFive.put(a,num);
		});
		
			t5.stream().forEach(a -> {
				Long num = l.stream().filter(b -> b.getActivity().equals(a))
							.count();
				dur.put(a, num);
			});
			
			afis=t5.stream().filter(a->subFive.get(a)*100/dur.get(a)>=90)
					.collect(Collectors.toCollection(ArrayList::new));
			
			FileWriter writer = new FileWriter("task5.txt");
			afis.stream().forEach(a -> {
				try {
					writer.write(a + "\t");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			writer.close();
		}





	

	
	public static void main(String args[]) throws IOException{
		
		try {
			l=readFile();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long days=task1();
		String task2=task2().toString();
		try {
			PrintWriter writer=new PrintWriter("task2.txt","UTF-8");
			writer.println(task2);
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String task3=task3().toString();
		try {
			PrintWriter writer=new PrintWriter("task3.txt","UTF-8");
			writer.println(task3);
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		task4();
		task5();
		
		System.out.println(days);
		System.out.println(task2);
		System.out.println(task3);

		
	}
	
	

}
