package lambda;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
	
	private String startDate;
	private String startTime;
	private String endDate;
	private String endTime;
	private String activityLabel;
	private String s;
	
	public MonitoredData(String startDate, String startTime,String endDate,String endTime, String activityLabel){
		
		this.startDate=startDate;
		this.startTime=startTime;
		this.endDate=endDate;
		this.endTime=endTime;
		this.activityLabel=activityLabel;
		
	}
	
	public String getStartDate(){
		return startDate;
	}
	
	public String getStartTime(){
		return startTime;
	}
	
	
	public String getEndDate(){
		return endDate;
	}
	
	public String getEndTime(){
		return endTime;
	}
	
	public String getActivity(){
		return activityLabel;
	}
	
	public void setStartDate(String startDate){
		this.startDate=startDate;
	}
	
	public void setStartTime(String startTime){
		this.startTime=startTime;
	}
	
	public void setEndDate(String endDate){
		this.endDate=endDate;
	}
	
	public void setEnd(String endTime){
		this.endTime=endTime;
	}
	
	public void setActivity(String activityLabel){
		this.activityLabel=activityLabel;
	}
	
	public Date timeDate(String startDate,String startTime){
		s = startDate;
		s=s.concat(" ");
		s=s.concat(startTime);
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d = null;
		try {
			d = format.parse(s);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return d;
	}
	

	
	public int getDur(){
		int aux= (int) ((timeDate(endDate, endTime).getTime()-timeDate(startDate, startTime).getTime())/60000);

			return aux;

	}


	

}
